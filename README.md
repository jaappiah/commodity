# COMMODITY

A simple application that takes a CSV file of total commodities sold in Ghana and outputs a table.
Built entirely on liveview.

## Functionalities

  * Made use of gitlab's CI pipeline to ensure continous integration. A build and test jobs were created in the pipeline. All tests passed.
  * Containerized the application using a Dockerfile.
  * Saved the application image to google clooud build
  * Deployed to google kubernetes (GKE)
  * Used Recreate strategy for re-deployments. Better options can be pursued. 
