defmodule LivetestWeb.PageLiveTest.Unit do
  use LivetestWeb.ConnCase

  #import Phoenix.LiveViewTest
  alias LivetestWeb.PageLive

  def create_socket() do
    %{socket: %Phoenix.LiveView.Socket{}}
  end

  describe "commodity socket state" do
    setup do
      create_socket()
    end

    test "validate commidity parameters", %{socket: socket} do
      socket = PageLive.get_parameters(socket)

      assigns = socket.assigns
      assert length(assigns.years) == 22
      assert length(assigns.commodity_names) == 6
      assert is_nil(assigns.from)
      assert is_nil(assigns.to)
      assert assigns.commodity == "all"
    end

    test "validate successful state update", %{socket: socket} do
      socket = PageLive.get_parameters(socket)
      #lets create an update params
      params = %{"from" => "1998", "to" => "2017", "commodity" => "Gold"}
      socket = PageLive.run_and_assign_parameters(params, socket)

      assigns = socket.assigns
      assert assigns.commodity == "Gold"
      assert assigns.from == 1998
      assert assigns.loading
    end

    test "validate fail state update", %{socket: socket} do
      socket = PageLive.get_parameters(socket)
      #lets create an update params
      params = %{"from" => "1998", "to" => "", "commodity" => ""}
      socket = PageLive.run_and_assign_parameters(params, socket)

      assigns = socket.assigns
      assert assigns.commodity == "all"
      assert assigns.from == 1998
      assert is_nil(assigns.to)
      assert assigns.loading == false
    end
  end
end
