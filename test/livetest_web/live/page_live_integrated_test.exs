defmodule LivetestWeb.PageLiveTest.Integrated do
  use LivetestWeb.ConnCase

  import Phoenix.LiveViewTest

  alias LivetestWeb.PageLive

  describe "commodity integrated test" do
    test "disconnected and connected render", %{conn: conn} do
      {:ok, view, _disconnected_html} = live(conn, "/")

      html =
        view
        |> element("#view-selector-form")
        |> render_change(%{"date" => %{"from" => "1998", "to" => "2017", "commodity" => "Gold"}})

      assert html =~ "<option value=\"Gold\" selected=\"selected\">Gold</option>"
      assert html =~ "<div id=\"Gold\" class=\"loader\"></div>"
    end
  end
end
