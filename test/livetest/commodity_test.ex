defmodule Livetest.CommodityTest do
  use ExUnit.Case


  describe "commodities" do

    import Livetest.Utils.Commodity

    test "load commodity file" do
      commodity =
        get_commodities()
        |> Enum.to_list()

      assert length(commodity) > 0
    end

    test "get commodity years" do
      commodity = get_years()
      assert length(commodity) == 22
    end

    test "get-commodity-count" do
      names = get_commodity_names()
      assert length(names) == 6
    end

  end

  describe "commodity-helpers" do
    import Livetest.Utils.Helpers
    import Livetest.Utils.Commodity

    test "test-year-conversion-to-integer" do
      commodities =
        get_commodities()
        |> Enum.to_list()
        |> convert_date_string_to_integer()

      assert is_integer((List.first(commodities) |> List.first()))
    end

    test "commodity-table-fields" do
      commodities =
        get_commodities()
        |> Enum.to_list()
        |> convert_date_string_to_integer()
        |> return_table_fields(2000, 2010, ["Gold", "Crude Oil"])

      #IO.puts(inspect commodities, pretty: true)
      assert length(commodities) == 2
    end
  end
end
