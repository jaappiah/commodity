defmodule LivetestWeb.PageLive do
  use LivetestWeb, :live_view

  require Logger

  import Livetest.Utils.{Commodity, Helpers}

  @impl true
  def mount(_params, _session, socket) do
    {:ok,
      socket
      |> get_parameters()
    }
  end

  @impl true
  def handle_event("date", %{"date" => params}, socket) do
    socket = run_and_assign_parameters(params, socket)
    if socket.assigns.loading do
      send(self(), :run_analytics)
      {:noreply, assign(socket, dataset: [])}
    else
      {:noreply, socket}
    end
  end

  @impl true
  def handle_info(:run_analytics, %{assigns: %{from: from, to: to, commodity: commodity}} = socket) do
    Process.sleep(3000)
    #get commodity
    commodities =
      get_commodities()
      |> Enum.to_list()
      |> convert_date_string_to_integer()

    dataset =
      case commodity do
        "all" ->
          names = List.delete(socket.assigns.commodity_names, "all")
          Logger.debug(inspect names, pretty: true)
          return_table_fields(commodities, from, to, names)
        name ->
          return_table_fields(commodities, from, to, [name])
      end

    #Logger.debug(inspect dataset, pretty: true)
    {:noreply, assign(socket, dataset: dataset)}
  end

  def get_parameters(socket) do
    params = [
      years: get_years(),
      commodity_names: get_commodity_names(),
      from: nil,
      to: nil,
      commodity: "all",
      loading: false,
      dataset: []
    ]
    assign(socket, params)
  end

  def run_and_assign_parameters(params, socket) do
    cleaned_params = clean_range_parameters(params, socket)
    Keyword.values(cleaned_params)
    |> Enum.any?(&(is_nil(&1)))
    |> if do
      params = cleaned_params ++ [loading: false]
      assign(socket, params)
    else
      params = cleaned_params ++ [loading: true]
      assign(socket, params)
    end
  end
end
