defmodule Livetest.Utils.Commodity do
  @moduledoc """
  Handles commodity file
  """

  def get_years() do
    get_commodities()
    |> Enum.to_list()
    |> Enum.map(&(List.first/1))
    |> Enum.uniq()
  end

  def get_commodity_names() do
    names =
      get_commodities()
      |> Enum.to_list()
      |> Enum.map(&(Enum.at(&1, 1)))
      |> Enum.uniq()
    ["all"] ++ names
  end

  def get_commodities() do
    file = if System.get_env("MIX_ENV") == "prod", do: "./lib/livetest-0.1.0/priv/csv/commodity.csv", else: "priv/csv/commodity.csv"
    file
    |> File.stream!
    |> CSV.decode!()
  end
end
