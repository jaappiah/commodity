defmodule Livetest.Utils.Helpers do
  @moduledoc """
  helper functions comes here
  """

  @doc """
  takes parameters from form and decodes, returning a keyword list
  """
  def clean_range_parameters(params, socket) do
    from = if params["from"] == "", do: socket.assigns.from, else: String.to_integer(params["from"])
    to = if params["to"] == "", do: socket.assigns.to, else: String.to_integer(params["to"])
    commodity = if params["commodity"] == "", do: socket.assigns.commodity, else: params["commodity"]
    [
      from: from,
      to: to,
      commodity: commodity
    ]
  end

  @doc """
    takes string dates and convert them into integers
  """
  def convert_date_string_to_integer(commodities) do
    Enum.map(commodities, fn commodity ->
      integer_year = List.first(commodity) |> String.to_integer()
      List.replace_at(commodity, 0, integer_year)
    end)
  end

  @doc """
  takes a list of names and a range to get commodity data for display
  """
  def return_table_fields(commodities, from, to, names) do
    #filter and return dates that fall within from and to
    Enum.filter(commodities, fn commodity ->
      List.first(commodity) >= from and List.first(commodity) <= to
    end)
    |> Enum.group_by(&(Enum.at(&1, 1)))
    |> Enum.filter(fn {key, _values} -> key in names end)
  end

end
